//FirebaseESP8266.h must be included before ESP8266WiFi.h
#include <Arduino.h>
#include "FirebaseESP8266.h"
#include <ESP8266WiFi.h>

#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

#include <WiFiClient.h>

#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

#include<ESP8266WiFi.h>

#include <FastLED.h>

#define LED_ONBOARD 2
#define LED_RGB 5 //D2=>GPIO4
#define NUM_LEDS 3
#define RCWL 13
#define BUTTON 12
#define PIR 5

// #define LED_BRIGHTNESS 255
#define RED LED_BRIGHTNESS,0,0
#define BLUE 0,0,LED_BRIGHTNESS
#define DEEP_GREEN 0,LED_BRIGHTNESS,10
#define GREEN 0,LED_BRIGHTNESS,0
#define PURPLE LED_BRIGHTNESS,0,LED_BRIGHTNESS
#define PURPLE_DARK 50,0,255
#define ORANGE LED_BRIGHTNESS,LED_BRIGHTNESS,0
#define CYAN 0,LED_BRIGHTNESS,LED_BRIGHTNESS
#define WHITE LED_BRIGHTNESS,LED_BRIGHTNESS,LED_BRIGHTNESS
#define BLACK 0,0,0

#define FIREBASE_HOST "ajna-c92f6.firebaseio.com"
#define FIREBASE_AUTH "TyKahwoE4nabDMD5FhzhKMgnSXntkWrz54Scd2IE"

#define WIFI_SSID "Yuj"
#define WIFI_PASS "yuj12345"

#define OTA_VERSION_URL "http://192.168.1.101/fw.version"
#define OTA_BIN_URL "http://192.168.1.101/fw.bin"

//Define FirebaseESP8266 data object
FirebaseData firebaseData;

String path = "AJNA223344";

CRGB leds1[NUM_LEDS];

int CURRENT_VERSION=174;

int LED_BRIGHTNESS=255;

int ARMED_STATUS=1;

unsigned long t1=0,t2=0;

WiFiManager wifiManager;



int CHECK_VERSION() {
  WiFiClient client;

  HTTPClient http;

  Serial.print("[HTTP] begin...\n");
  if (http.begin(client, OTA_VERSION_URL)) {  // HTTP

    Serial.print("[HTTP] GET...\n");
    // start connection and send HTTP header
    int httpCode = http.GET();

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
        String payload = http.getString();
        Serial.println("Available version:"+payload);
        http.end();
        return(payload.toInt());
      }
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      http.end();
      // return(0);
    }

  } else {
    Serial.printf("[HTTP} Unable to connect\n");
    // return(0);
  }
  return(0);
}

void UPDATE_GO()
{
  // if ((WiFiMulti.run() == WL_CONNECTED)) {

    WiFiClient client;
    leds1[0]=CRGB(CYAN);
    FastLED.show();

    ESPhttpUpdate.setLedPin(LED_BUILTIN, LOW);

    t_httpUpdate_return ret = ESPhttpUpdate.update(client, OTA_BIN_URL);
    
    switch (ret) {
      case HTTP_UPDATE_FAILED:
        Serial.print("HTTP_UPDATE_FAILD Error (");                       //    THIS IS ONE SONGLE LINE
        Serial.print(ESPhttpUpdate.getLastError());                     //
        Serial.print("): ");                                            //
        Serial.println(ESPhttpUpdate.getLastErrorString().c_str());     //
        break;

      case HTTP_UPDATE_NO_UPDATES:
        Serial.println("HTTP_UPDATE_NO_UPDATES");
        break;

      case HTTP_UPDATE_OK:
        Serial.println("HTTP_UPDATE_OK");
        break;
    }
  // }
}

void WIFI_SETUP()
{
  wifiManager.setConnectTimeout(60);
  // wifiManager.setConfigPortalTimeout(120);
  wifiManager.autoConnect("AJNA8266");
  // wifiManager.
  // FastLED.show();
  WiFi.softAPdisconnect(true);
}

void FIREBASE_SETUP()
{
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);
}

void LED_BRIGHTNESS_UPDATE()
{
  Serial.print("Acquiring LED brightness data");
  Firebase.getInt(firebaseData, "/"+path+"/brightness");
  LED_BRIGHTNESS=firebaseData.intData();
}

void STATUS_UPDATE()
{
  LED_BRIGHTNESS_UPDATE();
  Serial.print("Attempt data read");
  Firebase.getInt(firebaseData, "/"+path+"/status");
  ARMED_STATUS=firebaseData.intData();
  ARMED_STATUS?(leds1[0]=CRGB(GREEN)):(leds1[0]=CRGB(PURPLE));
  FastLED.show();
}

void OTA_ROUTINE()
{
  Serial.println("Checking for update");
  if(CHECK_VERSION()>CURRENT_VERSION)
  {
    Serial.println("Updates available!  Initiating download...");
    UPDATE_GO();
  }
  else
  {
    Serial.println("No updates for now");
    delay(1000);
  }
}

void CHECK_INTRUSION()
{
  if(digitalRead(RCWL)&&digitalRead(PIR))
  {
    leds1[0]=CRGB(WHITE);
    STATUS_UPDATE();
    if(ARMED_STATUS)
    {
      Serial.println("Attempt data push");
      Firebase.setInt(firebaseData, "/"+path+"/detectedStatus",1);
      // Firebase.setInt(firebaseData, "/"+path+"/RCWL",1);
      // Firebase.setInt(firebaseData, "/"+path+"/PIR",1);
      Serial.println(firebaseData.errorReason());
    }
  }
  else if(ARMED_STATUS)
  {
      if (digitalRead(RCWL)&&(!digitalRead(PIR)))
    {
      leds1[0]=CRGB(GREEN);
      // Firebase.setInt(firebaseData, "/"+path+"/RCWL",1);
      // Firebase.setInt(firebaseData, "/"+path+"/PIR",0);
    }
    else if ((!digitalRead(RCWL))&&digitalRead(PIR))
    {
      leds1[0]=CRGB(GREEN);
      // Firebase.setInt(firebaseData, "/"+path+"/RCWL",0);
      // Firebase.setInt(firebaseData, "/"+path+"/PIR",1);
    }
    else if((digitalRead(RCWL)==0)&&(digitalRead(PIR)==0))
    {
      leds1[0]=CRGB(GREEN);
      // Firebase.setInt(firebaseData, "/"+path+"/RCWL",0);
      // Firebase.setInt(firebaseData, "/"+path+"/PIR",0);
    }
    else
    {
      leds1[0]=CRGB(PURPLE);
    }
  }
  FastLED.show();
}

void BUTTON_ROUTINE()
{
  if(!digitalRead(BUTTON))
  {
    // leds1[0]=CRGB(RED);
    if(millis()>t2+5000)
    {
      // leds1[0]=CRGB(PURPLE);
      wifiManager.setConfigPortalTimeout(60);
      wifiManager.startConfigPortal("AJNA8266");
      WiFi.softAPdisconnect(1);
    }
  }
  else
  {
    t2=millis();
  }
}

void setup()
{
  Serial.begin(115200);

  pinMode(LED_ONBOARD,OUTPUT);
  digitalWrite(LED_ONBOARD,LOW);
  // pinMode(LED_RGB,OUTPUT);
  pinMode(RCWL,INPUT);
  pinMode(PIR,INPUT);
  pinMode(BUTTON,INPUT);

  FastLED.addLeds<WS2812, LED_RGB, GRB>(leds1, NUM_LEDS);

  leds1[0]=CRGB(BLUE);
  FastLED.show();
  Serial.println("Boot sequence");
  
  WIFI_SETUP();

  FIREBASE_SETUP();

  LED_BRIGHTNESS_UPDATE();  
  OTA_ROUTINE();
}


void loop()
{
  BUTTON_ROUTINE();
  if(WiFi.status()!=WL_CONNECTED)
  {
    delay(1000);
    leds1[0]=CRGB(ORANGE);
    FastLED.show();
    delay(1000);
    // WIFI_SETUP();
  }
  else
  {
    CHECK_INTRUSION();
    if(millis()-t1>60000)
    {
      STATUS_UPDATE();
      CHECK_INTRUSION();
      t1=millis();
    }
    
    FastLED.show();
    // delay(100);
  }
}